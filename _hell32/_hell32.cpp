// _hell32.cpp : _hell32のエントリポイント
//

#include <windows.h>
#include <Shlobj.h>

FARPROC p_DragAcceptFiles;
FARPROC p_DragFinish;

//FARPROC p_DragQueryFileA;
typedef UINT (WINAPI *D_DragQueryFileA)(HDROP, UINT, LPSTR, UINT);
D_DragQueryFileA p_DragQueryFileA;

FARPROC p_SHBrowseForFolderA;
FARPROC p_SHFileOperationA;
FARPROC p_SHGetDataFromIDListA;
FARPROC p_SHGetDesktopFolder;
FARPROC p_SHGetFileInfoA;
FARPROC p_SHGetMalloc;

//FARPROC p_SHGetPathFromIDListA;
typedef BOOL (WINAPI *D_SHGetPathFromIDListA)(PCIDLIST_ABSOLUTE, LPSTR);
D_SHGetPathFromIDListA p_SHGetPathFromIDListA;

FARPROC p_SHGetSpecialFolderLocation;
FARPROC p_ShellExecuteA;
FARPROC p_Shell_NotifyIconA;

HINSTANCE h_original;

BOOL APIENTRY DllMain( HANDLE hModule, 
                    DWORD  ul_reason_for_call, 
                    LPVOID lpReserved
                  )
{
 switch( ul_reason_for_call )
 {
 case DLL_PROCESS_ATTACH:
     h_original = LoadLibraryA( "shell32.dll" );
     if ( h_original == NULL )
         return FALSE;
       p_DragAcceptFiles = GetProcAddress( h_original, "DragAcceptFiles" );
       p_DragFinish = GetProcAddress( h_original, "DragFinish" );
       p_DragQueryFileA = (D_DragQueryFileA)GetProcAddress( h_original, "DragQueryFileA" );
       p_SHBrowseForFolderA = GetProcAddress( h_original, "SHBrowseForFolderA" );
       p_SHFileOperationA = GetProcAddress( h_original, "SHFileOperationA" );
       p_SHGetDataFromIDListA = GetProcAddress( h_original, "SHGetDataFromIDListA" );
       p_SHGetDesktopFolder = GetProcAddress( h_original, "SHGetDesktopFolder" );
       p_SHGetFileInfoA = GetProcAddress( h_original, "SHGetFileInfoA" );
       p_SHGetMalloc = GetProcAddress( h_original, "SHGetMalloc" );
       p_SHGetPathFromIDListA = (D_SHGetPathFromIDListA)GetProcAddress( h_original, "SHGetPathFromIDListA" );
       p_SHGetSpecialFolderLocation = GetProcAddress( h_original, "SHGetSpecialFolderLocation" );
       p_ShellExecuteA = GetProcAddress( h_original, "ShellExecuteA" );
       p_Shell_NotifyIconA = GetProcAddress( h_original, "Shell_NotifyIconA" );
     break;
 case DLL_THREAD_ATTACH:
     break;
 case DLL_THREAD_DETACH:
     break;
 case DLL_PROCESS_DETACH:
     FreeLibrary( h_original );
     break;
 default:
     break;
 }
 return TRUE;
 }

#include <atlbase.h>
#include <atlconv.h>

__declspec( naked ) void d_DragAcceptFiles() { _asm{ jmp p_DragAcceptFiles } }
__declspec( naked ) void d_DragFinish() { _asm{ jmp p_DragFinish } }

//__declspec( naked ) void d_DragQueryFileA() { _asm{ jmp p_DragQueryFileA } }
UINT WINAPI d_DragQueryFileA(HDROP hDrop, UINT iFile, LPSTR lpszFile, UINT cch)
{
  //OutputDebugStringA("d_DragQueryFileA: ");
  int alen = 0;
  if(iFile==0xFFFFFFFF)return DragQueryFileA(hDrop, iFile, lpszFile, cch);
  
  UINT wlen = DragQueryFileW(hDrop, iFile, NULL, 0);
  wchar_t *tmp = (wchar_t *)malloc(sizeof(wchar_t)*wlen+2);
  wchar_t *shortpath = NULL;
  alen = DragQueryFileW(hDrop, iFile, tmp, wlen+1);
  if(alen==0)goto end_DragQueryFileA;//err
  
  DWORD dwShortPath = GetShortPathNameW(tmp, NULL, 0);
  shortpath = (wchar_t *)malloc(sizeof(wchar_t)*(dwShortPath+2));
  GetShortPathNameW(tmp, shortpath, dwShortPath+1);
  
  alen = lstrlenA(CW2A(shortpath));
  if(lpszFile==NULL)
  {
    goto end_DragQueryFileA;
  }
  //OutputDebugStringW(shortpath);
  lstrcpynA(lpszFile, CW2A(shortpath), cch+1);
  goto end_DragQueryFileA;
  
end_DragQueryFileA:
  if(tmp!=NULL)free(tmp);
  if(shortpath!=NULL)free(shortpath);
  return alen;
}

__declspec( naked ) void d_SHBrowseForFolderA() { _asm{ jmp p_SHBrowseForFolderA } }
__declspec( naked ) void d_SHFileOperationA() { _asm{ jmp p_SHFileOperationA } }
__declspec( naked ) void d_SHGetDataFromIDListA() { _asm{ jmp p_SHGetDataFromIDListA } }
__declspec( naked ) void d_SHGetDesktopFolder() { _asm{ jmp p_SHGetDesktopFolder } }
__declspec( naked ) void d_SHGetFileInfoA() { _asm{ jmp p_SHGetFileInfoA } }
__declspec( naked ) void d_SHGetMalloc() { _asm{ jmp p_SHGetMalloc } }

//__declspec( naked ) void d_SHGetPathFromIDListA() { _asm{ jmp p_SHGetPathFromIDListA } }
BOOL WINAPI d_SHGetPathFromIDListA(PCIDLIST_ABSOLUTE pidl, LPSTR pszPath)
{
  //OutputDebugStringA("d_SHGetPathFromIDListA");
  wchar_t *wpath = (wchar_t *)calloc(MAX_PATH+1, sizeof(wchar_t));
  BOOL ret = SHGetPathFromIDListEx(pidl, wpath, MAX_PATH, GPFIDL_ALTNAME);
  //BOOL ret = SHGetPathFromIDListW(pidl, wpath);
  if(ret==FALSE)
  {
    free(wpath);
    return FALSE;
  }
  //OutputDebugStringW(wpath);
  lstrcpyA(pszPath, CW2A(wpath));
  free(wpath);
  return TRUE;
}

__declspec( naked ) void d_SHGetSpecialFolderLocation() { _asm{ jmp p_SHGetSpecialFolderLocation } }
__declspec( naked ) void d_ShellExecuteA() { _asm{ jmp p_ShellExecuteA } }
__declspec( naked ) void d_Shell_NotifyIconA() { _asm{ jmp p_Shell_NotifyIconA } }
