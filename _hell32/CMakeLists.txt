cmake_minimum_required(VERSION 3.8)
project (_hell32)

add_definitions(-D_UNICODE -DUNICODE)

add_library(_hell32 SHARED _hell32.cpp _hell32.def)

set(CMAKE_SHARED_LINKER_FLAGS_RELEASE "${CMAKE_SHARED_LINKER_FLAGS_RELEASE} /OPT:REF /MANIFEST:NO")
